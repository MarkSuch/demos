from shapely.geometry import Point, Polygon
import json
from bs4 import BeautifulSoup

with open('Reporte Factibilidad BAFI 2.3 (LIMA) - W33AM.geojson') as f:
    data = json.load(f)
list_polygons = []
data_list = []
cont = 0
for item in data['features']:
    cont += 1
    data_flat = {}
    data_flat['type'] = ''
    data_flat['style'] = ''
    data_flat['near'] = ''
    data_flat['kpi_w'] = ''
    data_flat['ratio'] = ''
    data_flat['prb_dl '] = ''
    data_flat['used_layers'] = ''
    list_coordenadas = []

    data_flat['type'] = item['type']
    data_flat['shape'] = item['geometry']['type']

    for list_coords in item['geometry']['coordinates']:
        # list_coordenadas.append([(x[0], x[1]) for x in list_coords])
        data_flat['coordinates'] = [(x[0], x[1]) for x in list_coords]
    # data_flat['coordinates'] = list_coordenadas[0] if len(list_coordenadas) == 1 else 'null'

    data_flat['centroid_lat'] = Polygon(data_flat['coordinates']).centroid.y
    data_flat['centroid_lng'] = Polygon(data_flat['coordinates']).centroid.x
    data_flat['bounds'] = Polygon(data_flat['coordinates']).bounds
    data_flat['area'] = Polygon(data_flat['coordinates']).area

    data_flat['name'] = item['properties']['name']
    data_flat['style'] = item['properties']['styleUrl'].replace('#', '')
    data_flat['description'] = item['properties']['description']

    # data_flat['prb_dl '] = ''
    # data_flat['used_layers'] = ''

    html_description = BeautifulSoup(item['properties']['description'], "html.parser")
    for tds in html_description.find_all('tr'):
        if len(tds.find_all('td')) >= 2:
            td_1 = tds.find_all('td')[0].getText()
            td_tmp = None
            if 'kpi' in td_1.lower():
                td_tmp = tds.find_all('td')[1].getText().lower().replace('w', '')
                data_flat['kpi'] = td_tmp if len(td_tmp) > 0 else 'null'
            elif 'radio' in td_1.lower():
                td_tmp = tds.find_all('td')[1].getText().lower().replace('m', '').strip()
                data_flat['radio'] = td_tmp if len(td_tmp) > 0 else 'null'
            elif 'uso de red' in td_1.lower():
                td_tmp = tds.find_all('td')[1].getText().replace('%', '')
                data_flat['uso_de_red'] = td_tmp if len(td_tmp) > 0 else 'null'
            elif 'capas usadas' in td_1.lower():
                td_tmp = tds.find_all('td')[1].getText()
                data_flat['capas_usadas'] = td_tmp if len(td_tmp) > 0 else 'null'
            elif 'pci' in td_1.lower():
                td_tmp = tds.find_all('td')[1].getText()
                data_flat['pci'] = td_tmp if len(td_tmp) > 0 else 'null'
            else:
                print('no se encontro ninguna descricion válida')
        else:
            print('menos de dos columnas en una fila en la tabla de descripcion')

    data_list.append(data_flat)
cont = 0
with open('inserts_1.sql', 'w') as f:
    for x in data_list:
        cont += 1
        f.write(
            """# {}\n
                insert into americatel_coverage (name,shape,coordinates,file_id, description,f_activo,type,style,kpi_w,ratio,use_network,used_layers,pci,centroid_lat, centroid_lng, bounds, area)values('{}','{}','{}', {}, '{}', {}, '{}', '{}', {}, {}, {}, {}, {}, {}, {}, '{}', {});\n""".format(
                cont, x['name'], x['shape'], x['coordinates'], 3, x['description'], 1, x['type'], x['style'], x['kpi'],
                x['radio'],
                x['uso_de_red'],
                x['capas_usadas'],
                x['pci'],
                x['centroid_lat'],
                x['centroid_lng'],
                x['bounds'],
                x['area'],))
print(cont)