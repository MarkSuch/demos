"""


def cp_prox(request):
    response = dict()
    if request.GET.get('coordenadas') and request.GET.get('num_calc'):
        coordenadas = request.GET.get('coordenadas')
        num_calc = request.GET.get('num_calc')
        lat = coordenadas.split(',')[0]
        lng = coordenadas.split(',')[1]
        sql="""
            SELECT id,
               nombre,
               distrito_id,
               distrito,
               provincia_id,
               provincia,
               departamento_id,
               departamento,
               latitud,
               longitud,
               (6371 *
                acos(cos(radians(latitud)) * cos(radians(%s)) *
                     cos(radians(%s) - radians(longitud)) +
                     sin(radians(latitud)) * sin(radians(%s)))) AS distance
        FROM americatel_centropoblado
        HAVING distance < 10
        ORDER BY distance
        LIMIT 0 , %s;""" % (lat, lng, lat, num_calc)
        cursor = connection.cursor()
        cursor.execute(sql)
        response['result'] = list(dictfetchall(cursor))

        if request.GET.get('avg') == 'true':
            latitudes = list()
            longitudes = list()
            from decimal import Decimal
            for x in response['result']:
                latitudes.append(Decimal(x['latitud']))
                longitudes.append(Decimal(x['longitud']))
            avg_lat = sum(latitudes)/len(latitudes)
            avg_lng = sum(longitudes)/len(longitudes)
            response['avg'] = '%f,%f' % (avg_lat, avg_lng)

        response['code'] = 200
        cursor.close()
    else:
        response['result'] = 'parametros obligatorios: coordenadas y num_calc. opcionales: avg'
        response['code'] = 400
    res = HttpResponse(json.dumps(response), mimetype="application/json")
    res.headers = {'Content-Type':'application/json'}
    return res


#mapa de cobertura - Marko pendiente
def cobertura(request):
    return render_to_response('americatel/cobertura.html',{})

def cobertura_api(request):
    response = dict()
    if request.GET.get('latlng') and request.GET.get('medio'):
        p_coordenadas = request.GET.get('latlng')
        p_medio = request.GET.get('medio')
        lat = float(p_coordenadas.split(',')[0])
        lng = float(p_coordenadas.split(',')[1])
        sql = """select ac.id, ac.name, ac.coordinates, ac.style
                    from americatel_coverage ac
                                join americatel_coverage_file acf on ac.file_id = acf.id
                    where ac.shape = 'Polygon'
                      and acf.f_activo = 1
                      and ac.f_activo = 1
                      and acf.medio_id = %s;""" % p_medio
        cursor = connection.cursor()
        cursor.execute(sql)
        dataset = list(dictfetchall(cursor))
        list_polygons = [Polygon(ast.literal_eval(x['coordinates'])) for x in dataset]
        response['result'] = 0
        punto = Point(lng, lat)
        cont = 0
        shapes = []
        response['result'] = []
        for k, v in enumerate(list_polygons):
            if v.contains(punto):
                shape_tmp = {}
                shape_tmp['id'] = dataset[k]['id']
                shape_tmp['name'] = dataset[k]['name']
                shape_tmp['style'] = dataset[k]['style']
                shape_tmp['coordinates'] = ast.literal_eval(dataset[k]['coordinates'])
                shapes.append(shape_tmp)
                cont += 1

        response['count'] = cont
        response['result'] = shapes
        response['code'] = 200
        cursor.close()
    else:
        response['result'] = 'parametros obligatorios: coordenadas y num_calc. opcionales: avg'
        response['code'] = 400
    res = HttpResponse(json.dumps(response), mimetype="application/json")
    res.headers = {'Content-Type':'application/json'}
    return res


"""

"""
-- auto-generated definition
create table americatel_coverage
(
       id           int auto_increment
              primary key,
       name         varchar(100) default '' not null,
       shape        varchar(10) default ''  not null,
       coordinates  text                    not null,
       centroid_lat decimal(10, 8)          null,
       centroid_lng decimal(11, 8)          null,
       bounds       varchar(100)            null,
       description  varchar(500)            null,
       file_id      int                     not null,
       f_activo     int                     not null,
       type         varchar(20)             null,
       style        varchar(40)             null,
       near         varchar(300)            null,
       kpi_w        int                     null,
       ratio        int                     null,
       used_layers  int                     null,
       pci          int                     null,
       use_network  int                     null,
       area         decimal(30, 25)         null
);

create index americatel_coverage__file_fk
       on americatel_coverage (file_id);



-- auto-generated definition
create table americatel_coverage_file
(
       id               int auto_increment
              primary key,
       name             varchar(50) null,
       medio_id         int         not null,
       created_at       datetime    null,
       updated_at       datetime    null,
       f_activo         int         not null,
       user             int         not null,
       department       int         null,
       tecnologia_tx_id int         null
);


"""

"""

# -12.162350,-76.991053

SELECT id,
       name,
       style,
       ratio,
       area,
       ((6371 *
         acos(cos(radians(centroid_lat)) * cos(radians(-12.162350)) *
              cos(radians(-76.991053
                      ) - radians(centroid_lng)) +
              sin(radians(centroid_lat)) * sin(radians(-12.162350)))) * 1000) AS distance
FROM americatel_coverage
HAVING distance < 10000
ORDER BY distance
LIMIT 0 , 500;

select *
from americatel_coverage
where id in (636, 1071, 1657, 1438, 1374, 681, 321);

select ac.id,
       ac.name,
       ac.coordinates,
       ac.style,
       ac.description,
       (6371000 *
        acos(cos(radians(centroid_lat)) * cos(radians(-12.162350)) *
             cos(radians(-76.991053) - radians(centroid_lng)) +
             sin(radians(centroid_lat)) * sin(radians(-12.162350))))AS distance
from americatel_coverage ac
       join americatel_coverage_file acf on ac.file_id = acf.id
where ac.shape = 'Polygon'
  and acf.f_activo = 1
  and ac.f_activo = 1
  and acf.tecnologia_tx_id = 3
  and acf.id =
HAVING distance < 3000
order by distance;

#10000 -> 499
#5000  -> 252
#3000  -> 126

select *
from americatel_coverage
order by ratio desc;

select *
from americatel_coverage_file
where tecnologia_tx_id = 3
  and updated_at < now();

select id, ratio_max
from americatel_coverage_file
where tecnologia_tx_id = 3
order by updated_at desc
limit 1;

select *
from americatel_coverage_file;

# [(-76.981788, -12.046206), (-76.8936087637324, -12.0384913164682), (-76.902687153671, -12.0064800618642), (-76.9229428848674, -11.9800822790236), (-76.9515137222063, -11.9630281053716), (-76.984362464902, -11.9577273800899), (-77.0168474229595, -11.9649291219074), (-77.0443783109544, -11.9836156890456), (-76.981788, -12.046206)]

# p_lat = -12.162350
# p_lng = -76.991053
truncate table americatel_coverage;
DROP PROCEDURE IF EXISTS SP_GET_COVERAGE;

call SP_GET_COVERAGE(1, 3, 14, -12.162350, -76.991053);
CREATE PROCEDURE SP_GET_COVERAGE(IN p_flag INT, IN p_tecnologia_tx_id INT, IN p_department INT, IN p_lat DOUBLE,
                                 IN p_lng  DOUBLE)
  BEGIN
    DECLARE v_file_id INT;
    DECLARE v_ratio_max INT;

    # flag 1 si se quiere el ultimo reporte por la tecnologia dada
    IF p_flag = 1
    THEN
      select acf.id, acf.ratio_max
          into v_file_id, v_ratio_max
      from americatel_coverage_file acf
      where acf.tecnologia_tx_id = p_tecnologia_tx_id
        and acf.f_activo = 1
        and acf.department = p_department
      order by acf.updated_at desc
      limit 1;


      select ac.*, (6371000 *
                    acos(cos(radians(ac.centroid_lat)) *
                         cos(radians(p_lat)) *
                         cos(radians(p_lng) - radians(ac.centroid_lng)) +
                         sin(radians(ac.centroid_lat)) * sin(radians(p_lat))))AS distance
      from americatel_coverage ac
      where ac.shape = 'Polygon'
        and ac.f_activo = 1
        and ac.file_id = v_file_id
      HAVING distance < v_ratio_max
      order by distance;
    END IF;
  END;
  
  
  
  
  
  
  -- auto-generated definition
create table americatel_coverage
(
  id            int auto_increment
    primary key,
  name          varchar(100) default '' not null,
  shape         varchar(10) default ''  not null,
  coordinates   text                    not null,
  centroid_lat  decimal(10, 8)          null,
  centroid_lng  decimal(11, 8)          null,
  bounds        varchar(100)            null,
  description   varchar(500)            null,
  file_id       int                     not null,
  f_activo      int                     not null,
  type          varchar(20)             null,
  style         varchar(40)             null,
  near          varchar(300)            null,
  kpi_w         int                     null,
  ratio         int                     null,
  used_layers   int                     null,
  pci           int                     null,
  use_network   int                     null,
  area          decimal(30, 25)         null,
  intersects_id varchar(200)            null,
  neighbors_id  varchar(200)            null,
  constraint americatel_coverage__file_fk
  foreign key (file_id) references americatel_coverage_file (id)
);


-- auto-generated definition
create table americatel_coverage_file
(
  id               int auto_increment
    primary key,
  name             varchar(50) null,
  medio_id         int         not null,
  created_at       datetime    null,
  updated_at       datetime    null,
  f_activo         int         not null,
  user             int         not null,
  department       int         null,
  tecnologia_tx_id int         null,
  ratio_max        int         null
);


"""