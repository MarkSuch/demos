# import geopandas as gpd
# import numpy as np
from shapely.geometry import Point, Polygon
from shapely.ops import nearest_points
# from matplotlib.collections import PatchCollection
# import matplotlib

# from matplotlib.patches import Polygon

# import matplotlib.pyplot as plt
# fname = "assets/map.geojson"
#
# df = gpd.read_file(fname)
# df = df.geometry.ix[0]
# series = gpd.GeoSeries(df)
# print(series.centroid)
# # print(df.geometry)
# series.plot()
# print(series.length)
# print(series.total_bounds)

import json

with open('assets/geojson/example.geojson') as f:
    data = json.load(f)
list_coordenadas = []
list_polygons = []

for item in data['features']:
    if item['geometry']['type'] == 'Polygon':
        for list_coords in item['geometry']['coordinates']:
            list_coordenadas.append([(x[0], x[1]) for x in list_coords])

print('===')
# for x in list_coordenadas:
#     print("""insert into americatel_coverage ('shape', 'points') values('{}','{}';)""".format('Polygon',str(x)))

with open('sql.sql', 'w') as f:
    for x in list_coordenadas:
        f.write("""insert into americatel_coverage (shape, points, file_id, f_activo) values('{}','{}',{}, {});""".format('Polygon',str(x), 1, 1))

list_polygons = [Polygon(x) for x in list_coordenadas]

p1 = Point(-77.112, -12.0406)
# p2 = Point(-77.10844,-11.96673)
p2 = Point(-77.108178, -11.966904)
cont = 0

# si punto esta en uno de los poligonos
for x in list_polygons:
    if x.contains(p2):
        print(x)
        # print(cont)
        # x = [x[0] for x in list_coordenadas[cont]]
        # y = [x[1] for x in list_coordenadas[cont]]
        # plt.plot(x, y, linewidth=1)
        # plt.show()
    # else:
    #     # si no esta en un poligono cual es el mas cercano
    #     print([o.wkt for o in nearest_points(p2, x)])
    # cont += 1

# print(list_polygons[0].exterior.xy)

#
