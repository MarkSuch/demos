from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import desc
from sqlalchemy import text

# engine = create_engine("mysql://mmena.e:D1a2G9pr3c10r@192.168.62.119/saf_pruebas")
engine = create_engine("mysql://root:@localhost/americatel")

Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()


class CentroPoblado(Base):
    __tablename__ = 'americatel_centropoblado'
    id = Column(Integer, primary_key=True)
    nombre = Column(String)
    departamento = Column(String)
    provincia = Column(String)
    distrito = Column(String)
    latitud = Column(String)
    longitud = Column(String)

    def __str__(self):
        return str(
            self.id) + '#' + self.nombre + ' ' + self.distrito + ' ' + self.provincia + ' ' + self.departamento + ' PERU|' + self.latitud + ',' + self.longitud


cont = 0
# centros = session.query(CentroPoblado).filter(CentroPoblado.departamento == "MADRE DE DIOS").order_by(
#     desc(CentroPoblado.id))
# for x in centros:
#     print(x)
#     cont += 1
# print(cont)
# session.add(myobject)
# session.commit()

f = open("data.sql", "r", encoding="utf-8")

for x in f:
    print(cont + 1)
    if x.strip() != '':
        session.execute(text("INSERT INTO MY_TABLE(id, direccion, distrito, provincia, departamento, pais, referencia, db_centro_poblado) VALUES (689, 'AV. JORGE BASADRE 130','San Isidro', 'Lima', 'Lima', 'Peru', null, '68972#SAN ISIDRO|SAN ISIDRO|SAN ISIDRO|LIMA|LIMA#-12.0991,-77.03463');"))
        # if cont == 100:
        #     break
        session.commit()
    print(x)
    cont += 1
session.commit()
