import requests
from os import system
import json
from opencage.geocoder import OpenCageGeocode
from geopy.geocoders import Nominatim
from geopy.distance import geodesic, great_circle
from utils import get_distance

key_opencage = '6cd6d4e835ce4ea4be964f6532913dbe'
key_google = 'AIzaSyDMXkV6UjKankZEOe6w_drp8WYKEcg5oek'
key_bing = 'AkvGsK9cGSgDGlVcvoo9F7fsMmcSoBVMaN_OY_rAThncuTE-TAIzP6a0bHZC5H6c'

geocoder = OpenCageGeocode(key_opencage)

lats = []
lngs = []

system("cls")

all_address = '76463#Bajada dgg Accomarca Vilcas Huaman Ayacucho, PERU#-12.59742|-69.18761'
list_all_address = all_address.lower().split('#')
address = list_all_address[1].lower().replace(' ', '%20')
print('address: ', list_all_address[1], '\n')
lat_db = float(list_all_address[2].split('|')[0])
long_db = float(list_all_address[2].split('|')[1])

# API GOOGLE
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
endpoint_google = "https://maps.googleapis.com/maps/api/geocode/json?address=%27{}%27&key={}".format(
    address, key_google)
response_google = requests.get(endpoint_google, headers=headers)
if response_google.status_code == 200:
    data_google = json.loads(response_google.text)
    latlng_google = [y['location'] for x, y in data_google['results'][0].items() if 'geometry' in x][0]
    lat_google = latlng_google['lat']
    lng_google = latlng_google['lng']
    lats.append(lat_google)
    lngs.append(lng_google)
    print('1 google', latlng_google)
    print('dist with google: ', get_distance(lat_db, long_db, lat_google, lng_google) / 1000, ' km')
    print('map: https://www.google.com.pe/maps/@{},{},15z'.format(latlng_google['lat'], latlng_google['lng']))
else:
    print('error en la respuesta del geocoder google')

# API OPENCAGE
response_opencage = geocoder.geocode(address)
if response_opencage:
    latlng_opencage = response_opencage[0]['geometry']
    lats.append(latlng_opencage['lat'])
    lngs.append(latlng_opencage['lng'])
    print('2 opencage', latlng_opencage)
    print('dist with opencage: ', get_distance(lat_db, long_db, latlng_opencage['lat'], latlng_opencage['lng']) / 1000,
          ' km')
    print('map: https://www.google.com.pe/maps/@{},{},15z'.format(latlng_opencage['lat'], latlng_opencage['lng']))
else:
    print('error en la respuesta del geocoder opencage')

# API BING
endpoint_bing = "http://dev.virtualearth.net/REST/v1/Locations?q={}&key={}".format(address, key_bing)
response_bing = requests.get(endpoint_bing, headers=headers)
if response_bing.status_code == 200:
    data_bing = json.loads(response_bing.text)
    if data_bing['resourceSets'][0]['estimatedTotal'] > 0:
        latlng_bing = \
            [y for x, y in data_bing['resourceSets'][0]['resources'][0].items() if 'geocodePoints' in x][0][0][
                'coordinates']
        lats.append(latlng_bing[0])
        lngs.append(latlng_bing[1])
        print('3 bing ', latlng_bing)
        print('dist with bing: ', get_distance(lat_db, long_db, latlng_bing[0], latlng_bing[1]) / 1000, ' km')
        print('map: https://www.google.com.pe/maps/@{},{},15z'.format(latlng_bing[0], latlng_bing[1]))
    else:
        print('la api de bing no devolvió resultados')
else:
    print('error en la respuesta del geocoder bing')
# geopy
# geolocator = Nominatim(user_agent="Marko test")
# location = geolocator.geocode(address_api)
# print('geopy nominatim', location)
# newport_ri = (lat1, long1)
# cleveland_oh = (lat_google, lng_google)
# print('disctance geopy geodesic', geodesic(newport_ri, cleveland_oh).km)
# print('disctance geopy great_circle', great_circle(newport_ri, cleveland_oh).km)
# print(data_dict['results'])

print('avg lats: ', sum(lats) / len(lats))
print('avg lngs: ', sum(lngs) / len(lngs))
print('dist with avg: ', get_distance(lat_db, long_db, sum(lats) / 3, sum(lngs) / 3) / 1000)
print('map: https://www.google.com.pe/maps/@{},{},15z'.format(sum(lats) / 3, sum(lngs) / 3))
print('latlng db: {},{}'.format(lat_db, long_db))

# long2 = latlng_google['lng']
# lat2 = latlng_google['lat']

# r = 6371000
# c = pi / 180
# d = 2 * r * asin(
#     sqrt(sin(c * (lat2 - lat1) / 2) ** 2 + cos(c * lat1) * cos(c * lat2) * sin(c * (long2 - long1) / 2) ** 2))
# # resultado en metros y kilometros
# print('mt: ', d, ' km: ', d / 1000)
