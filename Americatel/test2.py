#!/usr/bin/env/python
# -*- coding: utf-8

import pyproj, sys
from os import system
from math import sin, cos, sqrt, asin, pi

system("cls")

print(
    "Elija la opción del elipsoide")

print("WGS84 = 1 GRS80 = 2 clrk66 = 3 intl = 4")

opcion = int(input("opción = ? "))

# long1 = -67.9766140755
# lat1 = 9.36642904389
# long2 = -67.4801781185
# lat2 = 9.48364826587
long1 = -75.57087
lat1 = -12.37767
long2 = -75.67712
lat2 = -12.41121

if opcion == 1:
    name_ellps = "WGS84"

elif opcion == 2:
    name_ellps = "GRS80"

elif opcion == 3:
    name_ellps = "clrk66"

elif opcion == 4:
    name_ellps = "intl"

else:
    print("No existe la opción")
    sys.exit()

long1, lat1 = (long1, lat1)
long2, lat2 = (long2, lat2)
geod = pyproj.Geod(ellps=name_ellps)
angle1, angle2, distance = geod.inv(long1, lat1, long2, lat2)

print("La distancia es %0.2f metros basada en el elipsoide de" % distance, name_ellps)

r = 6371000  # radio terrestre medio, en metros

c = pi / 180  # constante para transformar grados en radianes

# Fórmula de haversine
d = 2 * r * asin(
    sqrt(sin(c * (lat2 - lat1) / 2) ** 2 + cos(c * lat1) * cos(c * lat2) * sin(c * (long2 - long1) / 2) ** 2))

print("La distancia es %0.2f metros basada en la formula de haversine" % d)

print("La diferencia, en valor absoluto, es %0.2f" % abs(distance - d), "metros")

print("La diferencia, en valor porcentual, es %0.2f" % abs((distance - d) * 100 / distance), "%")
