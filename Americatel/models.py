from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class SucursalTest(Base):
    __tablename__ = 'americatel_sucursal_test'
    # __tablename__ = 'my_table'
    id = Column(Integer, primary_key=True)
    id_direccion = Column(Integer)
    direccion_suc = Column(String)
    distrito_id_suc = Column(Integer)
    latitud_suc = Column(String)
    longitud_suc = Column(String)
    f_activo_suc = Column(Integer)
    numero_suc = Column(String)
    manzana_suc = Column(String)
    lote_suc = Column(String)
    referencia_suc = Column(String)
    id_centro_poblado_cp = Column(Integer)
    nombre_centro_poblado_cp = Column(String)
    distrito_id_cp = Column(Integer)
    distrito_cp = Column(String)
    provincia_id_cp = Column(Integer)
    provincia_cp = Column(String)
    departamento_id_cp = Column(Integer)
    departamento_cp = Column(String)
    latitud_cp = Column(String)
    longitud_cp = Column(String)
    ubigeo_cp = Column(String)
    f_activo_cp = Column(Integer)
    latitud_google = Column(String)
    longitud_google = Column(String)
    search_google = Column(String)
    status_google = Column(Integer)
    dist_google_db = Column(String)
    response_google = Column(String)
    latitud_opencage = Column(String)
    longitud_opencage = Column(String)
    search_opencage = Column(String)
    status_opencage = Column(Integer)
    dist_opencage_db = Column(String)
    response_opencage = Column(String)
    latitud_bing = Column(String)
    longitud_bing = Column(String)
    search_bing = Column(String)
    status_bing = Column(Integer)
    dist_bing_db = Column(String)
    response_bing = Column(String)

    def __str__(self):
        return str(
            self.id_direccion) + '#' + self.direccion_suc + '|' + self.distrito_cp + '|' + self.provincia_cp + '|' + self.departamento_cp + '|PERU#' + self.latitud_cp + ',' + self.longitud_cp


class SucursalDBProduccion(Base):
    __tablename__ = 'americatel_sucursal_tmp'
    id = Column(Integer, primary_key=True)
    cliente_id = Column(Integer)
    distrito_id = Column(Integer)
    centropoblado_id = Column(Integer)
    direccion = Column(String)
    descripcion = Column(String)
    referencia = Column(String)
    f_principal = Column(Integer)
    fech_modif = Column(DateTime)
    f_correspondencia = Column(Integer)
    tipodireccion_id = Column(Integer)
    tipovia_id = Column(Integer)
    nombrevia = Column(String)
    numero = Column(String)
    manzana = Column(String)
    lote = Column(String)
    tipointerior_id = Column(Integer)
    numerointerior = Column(String)
    organizacion_id = Column(Integer)
    nombreorganizacion = Column(String)
    urbanizacion = Column(String)
    sector_id = Column(Integer)
    nombresector = Column(String)
    usuario_edicion_id = Column(Integer)
    latitud = Column(String)
    longitud = Column(String)
    f_activo = Column(Integer)

    latitud_google = Column(String)
    longitud_google = Column(String)
    search_google = Column(String)
    status_google = Column(Integer)
    dist_google_db = Column(String)
    response_google = Column(String)
    latitud_opencage = Column(String)
    longitud_opencage = Column(String)
    search_opencage = Column(String)
    status_opencage = Column(Integer)
    dist_opencage_db = Column(String)
    response_opencage = Column(String)
    latitud_bing = Column(String)
    longitud_bing = Column(String)
    search_bing = Column(String)
    status_bing = Column(Integer)
    dist_bing_db = Column(String)
    response_bing = Column(String)

    f_geolocalizacion = Column(Integer)

    def __str__(self):
        return self.direccion + '#' + self.nombre


class CentroPoblado(Base):
    __tablename__ = 'americatel_centropoblado'
    id = Column(Integer, primary_key=True)
    departamento = Column(String)
    provincia = Column(String)
    distrito = Column(String)
    ubigeo = Column(String)
    ubi_dep = Column(String)
    ubi_pro = Column(String)
    ubi_dis = Column(String)
    nombre = Column(String)
    ubigeo_cp = Column(Integer)
    longitud = Column(Integer)
    latitud = Column(Integer)
    departamento_id = Column(Integer)
    provincia_id = Column(Integer)
    distrito_id = Column(Integer)
    fecha_modificacion = Column(DateTime)
    f_activo = Column(Integer)

    def __str__(self):
        return str(self.id) + '#' + self.distrito + '|' + self.provincia + '|' + self.departamento
        # return self.nombre


