from sqlalchemy import desc
from sqlalchemy import text
from utils import session, get_distance
from models import SucursalDBProduccion, CentroPoblado
import time
import re
import requests
import json
from opencage.geocoder import OpenCageGeocode

# key_opencage = '6cd6d4e835ce4ea4be964f6532913dbe' #primario marco.menatrejo
# key_opencage = 'a3429f2d7ebe480bb9e1516ffc7b18b2'  # secundario markhsuch
# key_opencage = '933c7652c009405fb4ebc17e45b03c77'  # con mail marko summit
key_opencage = '9d13e929889243dc91c0ad10596d05a1'  # con mail daniel gmail
key_bing = 'AkvGsK9cGSgDGlVcvoo9F7fsMmcSoBVMaN_OY_rAThncuTE-TAIzP6a0bHZC5H6c'
geocoder = OpenCageGeocode(key_opencage)

headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

# APIS = (1,)
# APIS = (2,)
# APIS = (3,)
# APIS = (1, 2)
# APIS = (2, 3)
APIS = (1, 3)
# APIS = (1, 2, 3)

rx_chars_repeats = re.compile(r'(.)\1{9,}')
cont = 0
queryset_cp = session.query(CentroPoblado).filter(CentroPoblado.f_activo == 1)
queryset = session.query(SucursalDBProduccion).filter(SucursalDBProduccion.status_google == 1)
latitud_cp = None
longitud_cp = None

for row in queryset:
    print('iter: ', cont + 1)
    if not rx_chars_repeats.search(row.direccion.strip()):
        list_columns = list()
        list_columns.append(row.direccion)
        for cp in queryset_cp:
            if cp.id == row.centropoblado_id:
                print('cp.id: ', cp.id, ' = ', 'suc.cp_id: ', row.centropoblado_id)
                # list_columns.append(str(row.numero_suc))
                list_columns.append(cp.nombre)
                list_columns.append(cp.distrito)
                list_columns.append(cp.provincia)
                list_columns.append(cp.departamento)
                list_columns.append('Peru')
                latitud_cp = cp.latitud
                longitud_cp = cp.longitud
                break
        # list_columns.append(row.latitud_cp)
        # list_columns.append(row.longitud_cp)
        list_columns = [x if x != 'None' else '' for x in list_columns]
        # if not row.numero_suc:
        #     print('no hay numero')
        # else:
        #     print('numero: ',list_columns[1])
        adr_concat_html = ' '.join(list_columns)
        adr_concat_html = adr_concat_html.replace('#', ' ')
        adr_concat = ' '.join(list_columns).replace('#', ' ').lower()
        adr_concat_html = adr_concat_html.lower().replace(' ', '%20').replace('#', ' ')
        print(adr_concat)

        # API GOOGLE
        if 1 in APIS:
            key_google = 'AIzaSyDMXkV6UjKankZEOe6w_drp8WYKEcg5oek'
            try:
                endpoint_google = "https://maps.googleapis.com/maps/api/geocode/json?address=%27{}%27&key={}".format(
                    adr_concat_html, key_google)
                response_google = requests.get(endpoint_google, headers=headers)
                if response_google.status_code == 200:
                    data_google = json.loads(response_google.text)
                    latlng_google = [y['location'] for x, y in data_google['results'][0].items() if 'geometry' in x][0]
                    lat_google = latlng_google['lat']
                    lng_google = latlng_google['lng']
                    distance_google = get_distance(float(latitud_cp), float(longitud_cp), lat_google,
                                                   lng_google)
                    print('1 google', latlng_google)
                    print('dist with google: ',
                          get_distance(float(latitud_cp), float(longitud_cp), lat_google, lng_google) / 1000,
                          ' km')
                    print(
                        'map: https://www.google.com.pe/maps/@{},{},15z'.format(latlng_google['lat'],
                                                                                latlng_google['lng']))
                else:
                    print('error en la respuesta del geocoder google')

                row.latitud_google = str(lat_google)
                row.longitud_google = str(lng_google)
                row.dist_google_db = str(distance_google)
                row.search_google = adr_concat
                row.response_google = response_google.text
                row.status_google = 4
            except Exception:
                row.status_google = 0

        # API OPENCAGE
        if 2 in APIS:
            try:
                response_opencage = geocoder.geocode(adr_concat)
                if len(response_opencage) > 0:
                    latlng_opencage = response_opencage[0]['geometry']
                    print('2 opencage', latlng_opencage)
                    distance_opencage = get_distance(float(row.latitud_cp), float(row.longitud_cp),
                                                     latlng_opencage['lat'],
                                                     latlng_opencage['lng'])
                    print('dist with opencage: ',
                          get_distance(float(row.latitud_cp), float(row.longitud_cp), latlng_opencage['lat'],
                                       latlng_opencage['lng']) / 1000,
                          ' km')
                    print(
                        'map: https://www.google.com.pe/maps/@{},{},15z'.format(latlng_opencage['lat'],
                                                                                latlng_opencage['lng']))
                else:
                    print('error en la respuesta del geocoder opencage')

                row.latitud_opencage = str(latlng_opencage['lat'])
                row.longitud_opencage = str(latlng_opencage['lng'])
                row.dist_opencage_db = str(distance_opencage)
                row.search_opencage = adr_concat
                row.response_opencage = str(response_opencage)
                row.status_opencage = 4
            except Exception:
                row.status_opencage = 0

        # API BING
        if 3 in APIS:
            try:
                endpoint_bing = "http://dev.virtualearth.net/REST/v1/Locations?q={}&key={}".format(adr_concat_html,
                                                                                                   key_bing)
                response_bing = requests.get(endpoint_bing, headers=headers)
                if response_bing.status_code == 200:
                    data_bing = json.loads(response_bing.text)
                    if data_bing['resourceSets'][0]['estimatedTotal'] > 0:
                        latlng_bing = \
                            [y for x, y in data_bing['resourceSets'][0]['resources'][0].items() if
                             'geocodePoints' in x][0][
                                0][
                                'coordinates']
                        print('3 bing ', latlng_bing)
                        distance_bing = get_distance(float(latitud_cp), float(longitud_cp), latlng_bing[0],
                                                     latlng_bing[1])
                        print('dist with bing: ',
                              get_distance(float(latitud_cp), float(longitud_cp), latlng_bing[0],
                                           latlng_bing[1]) / 1000, ' km')
                        print('map: https://www.google.com.pe/maps/@{},{},15z'.format(latlng_bing[0], latlng_bing[1]))
                    else:
                        print('la api de bing no devolvió resultados')
                else:
                    print('error en la respuesta del geocoder bing')

                row.latitud_bing = str(latlng_bing[0])
                row.longitud_bing = str(latlng_bing[1])
                row.dist_bing_db = str(distance_bing)
                row.search_bing = adr_concat
                row.response_bing = str(response_bing.text)
                row.status_bing = 4
            except Exception:
                row.status_bing = 0

        session.add(row)
        # time.sleep(1)
        session.commit()
        cont += 1
print(cont)

# session.commit()
