from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from math import sin, cos, sqrt, asin, pi

engine = create_engine("mysql://mmena.e:D1a2G9pr3c10r@192.168.62.119/saf_pruebas?charset=utf8&use_unicode=1",
                       encoding='utf-8')
# engine = create_engine("mysql://root:@localhost/americatel?charset=utf8&use_unicode=1", encoding='utf-8')
Session = sessionmaker(bind=engine)
session = Session()


def get_distance(lat1, long1, lat2, long2):
    r = 6371000  # radio terrestre medio, en metros
    c = pi / 180  # constante para transformar grados en radianes
    # Fórmula de haversine
    return 2 * r * asin(
        sqrt(sin(c * (lat2 - lat1) / 2) ** 2 + cos(c * lat1) * cos(c * lat2) * sin(c * (long2 - long1) / 2) ** 2))
