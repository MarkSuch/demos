from math import radians, cos, sin, asin, sqrt


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * asin(sqrt(a))
    r = 6371  # Radius of earth in kilometers. Use 3956 for miles
    return c * r

"""

(6371 *
        acos(cos(radians(latitud_cp)) * cos(radians(-9.039788593687323)) *
             cos(radians(-78.52050813751055) - radians(longitud_cp)) +
             sin(radians(latitud_cp)) * sin(radians(-9.039788593687323)))) AS distance
"""

# -12.00243538	-76.96585680

lista = [(-76.981788, -12.046206), (-76.8936087637324, -12.0384913164682), (-76.902687153671, -12.0064800618642), (-76.9229428848674, -11.9800822790236), (-76.9515137222063, -11.9630281053716), (-76.984362464902, -11.9577273800899), (-77.0168474229595, -11.9649291219074), (-77.0443783109544, -11.9836156890456), (-76.981788, -12.046206)]
# ratio
print(haversine(-76.981788, -12.046206, -76.8936087637324, -12.0384913164682)*1000)
# antena a centro
print(haversine(-76.981788, -12.046206, -76.96585680, -12.00243538)*1000)
# centro a punto maximo de radio arriba
print(haversine(-76.96585680, -12.00243538, -76.8936087637324, -12.0384913164682)*1000)
# centro a punto maximo de radio abajo
print(haversine(-76.96585680, -12.00243538, -77.0443783109544, -11.9836156890456)*1000)
print('=========')
ratios_list = []
for x in lista:
    # del centro a cada punta
    ratios_list.append(haversine(-76.96585680, -12.00243538, x[0], x[1]) * 1000)
print(max(ratios_list))


import pyproj
import shapely
import shapely.ops as ops
from shapely.geometry.polygon import Polygon
from functools import partial


geom = Polygon([(0, 0), (0, 10), (10, 10), (10, 0), (0, 0)])
geom_area = ops.transform(
    partial(
        pyproj.transform,
        pyproj.Proj(init='EPSG:4326'),
        pyproj.Proj(
            proj='aea',
            lat1=geom.bounds[1],
            lat2=geom.bounds[3])),
    geom)

# Print the area in m^2
print (geom_area.area)