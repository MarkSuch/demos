# -*- coding: utf-8 -*-
from matplotlib import pyplot as plt
from descartes import PolygonPatch
from shapely.geometry.polygon import LinearRing, Polygon
import json
import time
with open('assets/map.geojson') as f:
    data = json.load(f)
# cont = 0
x = []
y = []
for feature in data['features']:
    # print(feature['geometry']['type'])
    for i in feature['geometry']['coordinates']:
        for l in i:
            # print(l)
            x.append(l[0])
            y.append(l[1])
            # time.sleep(2)
    # print(feature['geometry']['coordinates'])
    # cont += 1
# # print(x)
# z = [[-77.069632, -12.015996, 0], [-77.070917, -12.015987, 0], [-77.070924, -12.016891, 0], [-77.071475, -12.016887, 0],
#      [-77.071482, -12.01779, 0], [-77.070931, -12.017794, 0], [-77.070938, -12.018698, 0], [-77.070571, -12.018701, 0],
#      [-77.070575, -12.019243, 0], [-77.070942, -12.01924, 0], [-77.070965, -12.022313, 0], [-77.070598, -12.022316, 0],
#      [-77.070595, -12.021954, 0], [-77.070045, -12.021958, 0], [-77.07004, -12.021416, 0], [-77.069122, -12.021423, 0],
#      [-77.069115, -12.020519, 0], [-77.068748, -12.020522, 0], [-77.068734, -12.018714, 0], [-77.069102, -12.018712, 0],
#      [-77.069088, -12.016904, 0], [-77.069639, -12.0169, 0], [-77.069632, -12.015996, 0]]

# x = [x[0] for x in z]
# y = [x[1] for x in z]
# # x = (4, 8, 13, 17, 20)
# # y = (54, 67, 98, 78, 45)
plt.plot(x, y, linewidth=1)
# plt.scatter(x, y)
plt.show()
