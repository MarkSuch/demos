"""
SET lc_time_names = 'es_MX';
### 1 Cantidad de ticket al mes contados por la fecha de recepcion/despacho de log�stica

select recibidos.anio                                            as anio,
       recibidos.mes                                             as mes,
       recibidos.cantidad_tickets                                as tickets_recibidos,
       despachados.cantidad_tickets                              as tickets_despachados,
       despachados.cantidad_tickets / recibidos.cantidad_tickets as ratio
from (select year(intermedia1.fecha_inicio)           as anio,
             date_format(intermedia1.fecha_fin, '%M') as mes,
             count(intermedia1.ticket_id)             as cantidad_tickets
      from (select tt.ticket_id as ticket_id, tt.fecha_inicio as fecha_inicio, tt.fecha_fin as fecha_fin
            from logistica_transicion_ticket tt
            where tt.estado_id = 2
              and tt.fecha_inicio between '2018-01-01' and '2018-12-31' #and tt.ticket_id in (77,79,191)
           ) as intermedia1
      group by year(intermedia1.fecha_inicio), month(intermedia1.fecha_inicio)) as recibidos
            join (select year(intermedia2.fecha_inicio)           as anio,
                         date_format(intermedia2.fecha_fin, '%M') as mes,
                         count(intermedia2.ticket_id)             as cantidad_tickets
                  from (select tt.ticket_id as ticket_id, tt.fecha_inicio as fecha_inicio, tt.fecha_fin as fecha_fin
                        from logistica_transicion_ticket tt
                        where tt.estado_id = 2
                          and tt.fecha_fin between '2018-01-01' and '2018-12-31' #and tt.ticket_id in (77,79,191)
                       ) as intermedia2
                  group by year(intermedia2.fecha_fin), month(intermedia2.fecha_fin)) as despachados
         on despachados.mes = recibidos.mes and despachados.anio = recibidos.anio;

### 2 Cantidad de familias al mes en total sin importar si estan dentro de uno o varios elementos
#validar con ticket_id in (117,166,197)
select year(t.fecha_creacion)              as anio,
       date_format(t.fecha_creacion, '%M') as mes,
       fet.familia                         as familia,
       count(fet.familia)                  as cantidad_familias
from logistica_ticket t
            inner join logistica_contenedor_ticket ct on t.id = ct.ticket_id
            inner join logistica_contenedor_ticket_equipo cte on cte.contenedor_ticket_id = ct.id and cte.f_activo
            inner join logistica_familia_equipo_ticket fet on fet.id = cte.equipo_ticket_id and fet.f_activo
    #where t.id in (117,166,197)
group by year(t.fecha_creacion), date_format(t.fecha_creacion, '%M'), fet.familia
order by year(t.fecha_creacion), month(t.fecha_creacion), fet.familia;


### 3 Cantidad de tickets por medio
select year(t.fecha_creacion)              as anio,
       date_format(t.fecha_creacion, '%M') as mes,
       m.nombre                            as medio,
       count(t.medio_id)                   as cantidad
from logistica_ticket t
            inner join americatel_medio m on m.id = t.medio_id
where t.fecha_creacion between '2018-01-01' and '2018-12-31'
group by year(t.fecha_creacion), date_format(t.fecha_creacion, '%M'), t.medio_id
order by year(t.fecha_creacion) desc, date_format(t.fecha_creacion, '%M') desc, m.id asc;

### 4 Cantidad de familias por medio
select year(t.fecha_creacion)              as anio,
       date_format(t.fecha_creacion, '%M') as mes,
       m.nombre                            as medio,
       fet.familia                         as familia,
       count(fet.familia)                  as cantidad_familias
from logistica_ticket t
            inner join americatel_medio m on m.id = t.medio_id
            inner join logistica_contenedor_ticket ct on t.id = ct.ticket_id
            inner join logistica_contenedor_ticket_equipo cte on cte.contenedor_ticket_id = ct.id and cte.f_activo
            inner join logistica_familia_equipo_ticket fet on fet.id = cte.equipo_ticket_id and fet.f_activo
    #where t.id in (117,166,197)
group by year(t.fecha_creacion), date_format(t.fecha_creacion, '%M'), m.id, fet.familia
order by year(t.fecha_creacion), month(t.fecha_creacion), m.id, fet.familia;

### 5 Cantidad de consultas por motivo
#(un ticket puede tener varios elementos y cada elemento puede tener el mismo motivo que se contar� por separado
select year(t.fecha_creacion)              as anio,
       date_format(t.fecha_creacion, '%M') as mes,
       mt.motivo                           as motivo,
       count(mt.motivo)                    as cantidad
from logistica_ticket t
            inner join logistica_contenedor_ticket ct on t.id = ct.ticket_id
            inner join logistica_comentario_motivo_contenedor cmc on cmc.contenedor_id = ct.id
            inner join logistica_motivo_ticket mt on mt.id = cmc.motivo_id
    #where t.id in (117,166,197)
group by year(t.fecha_creacion), date_format(t.fecha_creacion, '%M'), mt.motivo
order by year(t.fecha_creacion), month(t.fecha_creacion), 3;

### 6.1 Cantidad de consultas por familia y por medio
#(validar con el ticket 166 porque varias familias comparten el mismo motivo
select year(t.fecha_creacion)              as anio,
       date_format(t.fecha_creacion, '%M') as mes,
       m.nombre                            as medio,
       fet.familia                         as familia,
       mt.motivo                           as motivo,
       count(mt.motivo)                    as cantidad
from logistica_ticket t
            inner join americatel_medio m on m.id = t.medio_id
            inner join logistica_contenedor_ticket ct on t.id = ct.ticket_id
            inner join logistica_contenedor_ticket_equipo cte on cte.contenedor_ticket_id = ct.id and cte.f_activo
            inner join logistica_familia_equipo_ticket fet on fet.id = cte.equipo_ticket_id and fet.f_activo
            inner join logistica_comentario_motivo_contenedor cmc on cmc.contenedor_id = ct.id
            inner join logistica_motivo_ticket mt on mt.id = cmc.motivo_id
    #where t.id in (117,166,197)
group by year(t.fecha_creacion), date_format(t.fecha_creacion, '%M'), m.id, fet.id, mt.id
order by year(t.fecha_creacion), month(t.fecha_creacion), m.id, fet.familia;

### 6.2 Cantidad de consultas por familia
#(validar con el ticket 166 porque varias familias comparten el mismo motivo
select year(t.fecha_creacion)              as anio,
       date_format(t.fecha_creacion, '%M') as mes,
       fet.familia                         as familia,
       mt.motivo                           as motivo,
       count(mt.motivo)                    as cantidad
from logistica_ticket t
            inner join logistica_contenedor_ticket ct on t.id = ct.ticket_id
            inner join logistica_contenedor_ticket_equipo cte on cte.contenedor_ticket_id = ct.id and cte.f_activo
            inner join logistica_familia_equipo_ticket fet on fet.id = cte.equipo_ticket_id and fet.f_activo
            inner join logistica_comentario_motivo_contenedor cmc on cmc.contenedor_id = ct.id
            inner join logistica_motivo_ticket mt on mt.id = cmc.motivo_id
    #where t.id in (117,166,197)
group by year(t.fecha_creacion), date_format(t.fecha_creacion, '%M'), fet.id, mt.id
order by year(t.fecha_creacion), month(t.fecha_creacion), fet.familia;

### 7.1 Estado de la cantidad total de tickets
select year(t.fecha_creacion)              as anio,
       date_format(t.fecha_creacion, '%M') as mes,
       t.estado_id,
       et.descripcion,
       count(estado_id)                    as cantidad_tickets,
       case t.estado_id
              when 6 then count(estado_id) / (select count(id)
                                              from logistica_ticket
                                              where year(fecha_creacion) = year(t.fecha_creacion)
                                                and month(fecha_creacion) = month(t.fecha_creacion))
              else count(estado_id) / (select count(*)
                                       from logistica_ticket
                                       where estado_id != 6
                                         and year(fecha_creacion) = year(t.fecha_creacion)
                                         and month(fecha_creacion) = month(t.fecha_creacion))
           end                             as ratio
from logistica_ticket t
            inner join logistica_estado_ticket et on t.estado_id = et.id
group by year(t.fecha_creacion), month(t.fecha_creacion), t.estado_id;

select *
from logistica_ticket
where estado_id = 6;

### 7.3 Tiempo de atenci�n de ticket por contratista
select t.id as ticket, u.user as contratista, tt.*
from logistica_ticket t
            inner join americatel_usuario u on u.id = t.usuario_creador_id
            inner join logistica_transicion_ticket tt on tt.ticket_id = t.id
#where t.id in (117,166,197)
;

### 100 cantidad total de tickets creados por contratista por mes y a�o
select upper(u.user)                       as contratista,
       count(t.id)                         as tickets_x_contratista,
       date_format(t.fecha_creacion, '%M') as mes,
       year(t.fecha_creacion)              as anio
from logistica_ticket t
            inner join americatel_usuario u on u.id = t.usuario_creador_id
where t.fecha_creacion between '2018-01-01' and '2018-12-31'
group by month(t.fecha_creacion), u.id
order by year(t.fecha_creacion), month(t.fecha_creacion), u.user;

### 100.1 cantidad total de tickets creados por contratista por mes y a�o (solo los activos finalizados)
select upper(u.user)                       as contratista,
       count(t.id)                         as tickets_x_contratista,
       date_format(t.fecha_creacion, '%M') as mes,
       year(t.fecha_creacion)              as anio
from logistica_ticket t
            inner join americatel_usuario u on u.id = t.usuario_creador_id
where t.fecha_creacion between '2018-01-01' and '2018-12-31'
  and not t.f_anulado
  and t.estado_id = 5
group by month(t.fecha_creacion), u.id
order by year(t.fecha_creacion), month(t.fecha_creacion), u.user;

### 100.2 cantidad total de tickets creados por contratista separando por anulados por mes y a�o
select upper(u.user)                            as contratista,
       if(not t.f_anulado, 'activo', 'anulado') as estado,
       count(t.id)                              as tickets_x_contratista,
       date_format(t.fecha_creacion, '%M')      as mes,
       year(t.fecha_creacion)                   as anio
from logistica_ticket t
            inner join americatel_usuario u on u.id = t.usuario_creador_id
where t.fecha_creacion between '2018-01-01' and '2018-12-31'
group by month(t.fecha_creacion), u.id, t.f_anulado
order by year(t.fecha_creacion), month(t.fecha_creacion), u.user, t.f_anulado;


select *
from americatel_valortipochoice
where tipo = 37;
select *
from americatel_usuario
where user like 'mmujica';
select *
from logistica_ticket;
select *
from logistica_estado_ticket;
select ticket_id, count(ticket_id) as cantidad
from logistica_contenedor_ticket
group by ticket_id
order by cantidad desc;
select *
from logistica_transicion_ticket
where ticket_id in (117, 166, 197);
select *
from logistica_contenedor_ticket
where ticket_id = 117;
select *
from logistica_contenedor_ticket
where id = 272;
select contenedor_ticket_id, count(contenedor_ticket_id) as cantidad
from logistica_contenedor_ticket_equipo
group by contenedor_ticket_id
order by cantidad desc;
select *
from logistica_contenedor_ticket_equipo
where contenedor_ticket_id in (178, 180, 181, 182, 183, 184);
select *
from logistica_equipo_ticket
where id in (8, 19, 3, 15, 7, 7);
select *
from logistica_familia_equipo_ticket
where id in (3, 7, 8, 15, 19);
select *
from logistica_motivo_ticket;
select *
from logistica_comentario_motivo_contenedor;
select *
from logistica_contenedor_ticket;

"""




"""
def data_visualization(request):
    """
    :param request:
    :return:
    """

    response = dict()
    response['result'] = []
    res = None
    # return render_to_response('americatel/tickets_graphics.html',{})
    if request.method == 'GET':

        ### 1 Cantidad de ticket al mes contados por la fecha de recepcion/despacho de log�stica
        if request.GET.get('tickets_count') and request.GET.get('start_date') and request.GET.get('end_date'):

            p_tickets_count = request.GET.get('tickets_count').strip()
            p_start_date = request.GET.get('start_date').strip()
            p_end_date = request.GET.get('end_date').strip()

            query_1 = """select recibidos.anio                                            as anio,
                               recibidos.mes                                             as mes,
                               recibidos.cantidad_tickets                                as tickets_recibidos,
                               despachados.cantidad_tickets                              as tickets_despachados,
                               despachados.cantidad_tickets / recibidos.cantidad_tickets as ratio
                        from (select year(intermedia1.fecha_inicio)           as anio,
                                     date_format(intermedia1.fecha_fin, '%%M') as mes,
                                     count(intermedia1.ticket_id)             as cantidad_tickets
                              from (select tt.ticket_id as ticket_id, tt.fecha_inicio as fecha_inicio, tt.fecha_fin as fecha_fin
                                    from logistica_transicion_ticket tt
                                    where tt.estado_id = 2
                                      and tt.fecha_inicio between '2018-01-01' and '2018-12-31'
                                   ) as intermedia1
                              group by year(intermedia1.fecha_inicio), month(intermedia1.fecha_inicio)) as recibidos
                                    join (select year(intermedia2.fecha_inicio)           as anio,
                                                 date_format(intermedia2.fecha_fin, '%%M') as mes,
                                                 count(intermedia2.ticket_id)             as cantidad_tickets
                                          from (select tt.ticket_id as ticket_id, tt.fecha_inicio as fecha_inicio, tt.fecha_fin as fecha_fin
                                                from logistica_transicion_ticket tt
                                                where tt.estado_id = 2
                                                  and tt.fecha_fin between '2018-01-01' and '2018-12-31'
                                               ) as intermedia2
                                          group by year(intermedia2.fecha_fin), month(intermedia2.fecha_fin)) as despachados
                                 on despachados.mes = recibidos.mes and despachados.anio = recibidos.anio; """

            ### 2 Cantidad de familias al mes en total sin importar si estan dentro de uno o varios elementos
            query_2 = """select year(t.fecha_creacion)              as anio,
                               date_format(t.fecha_creacion, '%%M') as mes,
                               fet.familia                         as familia,
                               count(fet.familia)                  as cantidad_familias
                        from logistica_ticket t
                                    inner join logistica_contenedor_ticket ct on t.id = ct.ticket_id
                                    inner join logistica_contenedor_ticket_equipo cte on cte.contenedor_ticket_id = ct.id and cte.f_activo
                                    inner join logistica_familia_equipo_ticket fet on fet.id = cte.equipo_ticket_id and fet.f_activo
                            #where t.id in (117,166,197)
                        group by year(t.fecha_creacion), date_format(t.fecha_creacion, '%%M'), fet.familia
                        order by year(t.fecha_creacion), month(t.fecha_creacion), fet.familia;"""

            ### 3 Cantidad de tickets por medio
            query_3 = """select year(t.fecha_creacion)              as anio,
                               date_format(t.fecha_creacion, '%%M') as mes,
                               m.nombre                            as medio,
                               count(t.medio_id)                   as cantidad
                        from logistica_ticket t
                                    inner join americatel_medio m on m.id = t.medio_id
                        where t.fecha_creacion between '2018-01-01' and '2018-12-31'
                        group by year(t.fecha_creacion), date_format(t.fecha_creacion, '%%M'), t.medio_id
                        order by year(t.fecha_creacion) desc, date_format(t.fecha_creacion, '%%M') desc, m.id asc;"""

            ### 4 Cantidad de familias por medio
            query_4 = """select year(t.fecha_creacion)              as anio,
                               date_format(t.fecha_creacion, '%%M') as mes,
                               m.nombre                            as medio,
                               fet.familia                         as familia,
                               count(fet.familia)                  as cantidad_familias
                        from logistica_ticket t
                                    inner join americatel_medio m on m.id = t.medio_id
                                    inner join logistica_contenedor_ticket ct on t.id = ct.ticket_id
                                    inner join logistica_contenedor_ticket_equipo cte on cte.contenedor_ticket_id = ct.id and cte.f_activo
                                    inner join logistica_familia_equipo_ticket fet on fet.id = cte.equipo_ticket_id and fet.f_activo
                            #where t.id in (117,166,197)
                        group by year(t.fecha_creacion), date_format(t.fecha_creacion, '%%M'), m.id, fet.familia
                        order by year(t.fecha_creacion), month(t.fecha_creacion), m.id, fet.familia;"""

            ### 5 Cantidad de consultas por motivo
            # (un ticket puede tener varios elementos y cada elemento puede tener el mismo motivo que se contar� por separado
            query_5 = """select year(t.fecha_creacion)              as anio,
                               date_format(t.fecha_creacion, '%%M') as mes,
                               mt.motivo                           as motivo,
                               count(mt.motivo)                    as cantidad
                        from logistica_ticket t
                                    inner join logistica_contenedor_ticket ct on t.id = ct.ticket_id
                                    inner join logistica_comentario_motivo_contenedor cmc on cmc.contenedor_id = ct.id
                                    inner join logistica_motivo_ticket mt on mt.id = cmc.motivo_id
                            #where t.id in (117,166,197)
                        group by year(t.fecha_creacion), date_format(t.fecha_creacion, '%%M'), mt.motivo
                        order by year(t.fecha_creacion), month(t.fecha_creacion), 3;"""

            ### 6.1 Cantidad de consultas por familia y por medio
            query_6 = """select year(t.fecha_creacion)              as anio,
                               date_format(t.fecha_creacion, '%%M') as mes,
                               m.nombre                            as medio,
                               fet.familia                         as familia,
                               mt.motivo                           as motivo,
                               count(mt.motivo)                    as cantidad
                        from logistica_ticket t
                                    inner join americatel_medio m on m.id = t.medio_id
                                    inner join logistica_contenedor_ticket ct on t.id = ct.ticket_id
                                    inner join logistica_contenedor_ticket_equipo cte on cte.contenedor_ticket_id = ct.id and cte.f_activo
                                    inner join logistica_familia_equipo_ticket fet on fet.id = cte.equipo_ticket_id and fet.f_activo
                                    inner join logistica_comentario_motivo_contenedor cmc on cmc.contenedor_id = ct.id
                                    inner join logistica_motivo_ticket mt on mt.id = cmc.motivo_id
                            #where t.id in (117,166,197)
                        group by year(t.fecha_creacion), date_format(t.fecha_creacion, '%%M'), m.id, fet.id, mt.id
                        order by year(t.fecha_creacion), month(t.fecha_creacion), m.id, fet.familia;"""

            ### 6.2 Cantidad de consultas por familia
            query_6_2 = """select year(t.fecha_creacion)              as anio,
                               date_format(t.fecha_creacion, '%%M') as mes,
                               fet.familia                         as familia,
                               mt.motivo                           as motivo,
                               count(mt.motivo)                    as cantidad
                        from logistica_ticket t
                                    inner join logistica_contenedor_ticket ct on t.id = ct.ticket_id
                                    inner join logistica_contenedor_ticket_equipo cte on cte.contenedor_ticket_id = ct.id and cte.f_activo
                                    inner join logistica_familia_equipo_ticket fet on fet.id = cte.equipo_ticket_id and fet.f_activo
                                    inner join logistica_comentario_motivo_contenedor cmc on cmc.contenedor_id = ct.id
                                    inner join logistica_motivo_ticket mt on mt.id = cmc.motivo_id
                            #where t.id in (117,166,197)
                        group by year(t.fecha_creacion), date_format(t.fecha_creacion, '%%M'), fet.id, mt.id
                        order by year(t.fecha_creacion), month(t.fecha_creacion), fet.familia;"""

            ### 7.1 Estado de la cantidad total de tickets
            query_7_1 = """select year(t.fecha_creacion)              as anio,
                               date_format(t.fecha_creacion, '%%M') as mes,
                               t.estado_id,
                               et.descripcion,
                               count(estado_id)                    as cantidad_tickets,
                               case t.estado_id
                                      when 6 then count(estado_id) / (select count(id)
                                                                      from logistica_ticket
                                                                      where year(fecha_creacion) = year(t.fecha_creacion)
                                                                        and month(fecha_creacion) = month(t.fecha_creacion))
                                      else count(estado_id) / (select count(*)
                                                               from logistica_ticket
                                                               where estado_id != 6
                                                                 and year(fecha_creacion) = year(t.fecha_creacion)
                                                                 and month(fecha_creacion) = month(t.fecha_creacion))
                                   end                             as ratio
                        from logistica_ticket t
                                    inner join logistica_estado_ticket et on t.estado_id = et.id
                        group by year(t.fecha_creacion), month(t.fecha_creacion), t.estado_id;"""

            ### 7.3 Tiempo de atencion de ticket por contratista
            query_7_3 = """select t.id as ticket, u.user as contratista, tt.*
                            from logistica_ticket t
                                        inner join americatel_usuario u on u.id = t.usuario_creador_id
                                        inner join logistica_transicion_ticket tt on tt.ticket_id = t.id"""

            ### 100 cantidad total de tickets creados por contratista por mes y a�o
            query_100 = """select upper(u.user)                       as contratista,
                                   count(t.id)                         as tickets_x_contratista,
                                   date_format(t.fecha_creacion, '%%M') as mes,
                                   year(t.fecha_creacion)              as anio
                            from logistica_ticket t
                                        inner join americatel_usuario u on u.id = t.usuario_creador_id
                            where t.fecha_creacion between '2018-01-01' and '2018-12-31'
                            group by month(t.fecha_creacion), u.id
                            order by year(t.fecha_creacion), month(t.fecha_creacion), u.user;"""

            ### 100.1 cantidad total de tickets creados por contratista por mes y a�o (solo los activos finalizados)
            query_100_1 = """select upper(u.user)                       as contratista,
                                   count(t.id)                         as tickets_x_contratista,
                                   date_format(t.fecha_creacion, '%%M') as mes,
                                   year(t.fecha_creacion)              as anio
                            from logistica_ticket t
                                        inner join americatel_usuario u on u.id = t.usuario_creador_id
                            where t.fecha_creacion between '2018-01-01' and '2018-12-31'
                              and not t.f_anulado
                              and t.estado_id = 5
                            group by month(t.fecha_creacion), u.id
                            order by year(t.fecha_creacion), month(t.fecha_creacion), u.user;"""

            ### 100.2 cantidad total de tickets creados por contratista separando por anulados por mes y a�o
            query_100_2 = """select upper(u.user)                            as contratista,
                                   if(not t.f_anulado, 'activo', 'anulado') as estado,
                                   count(t.id)                              as tickets_x_contratista,
                                   date_format(t.fecha_creacion, '%%M')      as mes,
                                   year(t.fecha_creacion)                   as anio
                            from logistica_ticket t
                                        inner join americatel_usuario u on u.id = t.usuario_creador_id
                            where t.fecha_creacion between '2018-01-01' and '2018-12-31'
                            group by month(t.fecha_creacion), u.id, t.f_anulado
                            order by year(t.fecha_creacion), month(t.fecha_creacion), u.user, t.f_anulado;"""


            cursor_1 = connection.cursor()
            cursor_1.execute(query_1)
            dataset_1 = list(dictfetchall(cursor_1))

            cursor_2 = connection.cursor()
            cursor_2.execute(query_2)
            dataset_2 = list(dictfetchall(cursor_2))

            cursor_3 = connection.cursor()
            cursor_3.execute(query_3)
            dataset_3 = list(dictfetchall(cursor_3))

            cursor_4 = connection.cursor()
            cursor_4.execute(query_4)
            dataset_4 = list(dictfetchall(cursor_4))

            cursor_5 = connection.cursor()
            cursor_5.execute(query_5)
            dataset_5 = list(dictfetchall(cursor_5))

            cursor_6 = connection.cursor()
            cursor_6.execute(query_6)
            dataset_6 = list(dictfetchall(cursor_6))

            cursor_6_2 = connection.cursor()
            cursor_6_2.execute(query_6_2)
            dataset_6_2 = list(dictfetchall(cursor_6_2))

            cursor_7_1 = connection.cursor()
            cursor_7_1.execute(query_7_1)
            dataset_7_1 = list(dictfetchall(cursor_7_1))

            cursor_7_3 = connection.cursor()
            cursor_7_3.execute(query_7_3)
            dataset_7_3 = list(dictfetchall(cursor_7_3))

            cursor_100 = connection.cursor()
            cursor_100.execute(query_100)
            dataset_100 = list(dictfetchall(cursor_100))

            cursor_100_1 = connection.cursor()
            cursor_100_1.execute(query_100_1)
            dataset_100_1 = list(dictfetchall(cursor_100_1))

            cursor_100_2 = connection.cursor()
            cursor_100_2.execute(query_100_2)
            dataset_100_2 = list(dictfetchall(cursor_100_2))

            # data para el grafico 1
            tmp_list_1 = []
            tmp_list_2 = []
            for x in dataset_1:
                tmp_list_1.append(x['tickets_recibidos'])
                tmp_list_2.append(x['tickets_despachados'])

            response['result'] = {'chart_1': {
                'categories': [x['mes'] for x in dataset_1],
                'series': [{
                    'name': 'recibidos',
                    'data': tmp_list_1
                }, {
                    'name': 'atendidos',
                    'data': tmp_list_2
                }]}}

            # data para el grafico 2

            data_chart_2 = [{'cantidad_familias': 4L, 'anio': 2018L, 'familia': 'Antena RE', 'mes': 'July'},
                            {'cantidad_familias': 1L, 'anio': 2018L, 'familia': 'Axesat', 'mes': 'July'},
                            {'cantidad_familias': 4L, 'anio': 2018L, 'familia': 'Cisco', 'mes': 'July'},
                            {'cantidad_familias': 1L, 'anio': 2018L, 'familia': 'Fortigate', 'mes': 'July'},
                            {'cantidad_familias': 1L, 'anio': 2018L, 'familia': 'Fuentes', 'mes': 'July'},

                            {'cantidad_familias': 4L, 'anio': 2018L, 'familia': 'Antena RE', 'mes': 'Agosto'},
                            {'cantidad_familias': 1L, 'anio': 2019L, 'familia': 'Axesat', 'mes': 'Agosto'},
                            {'cantidad_familias': 4L, 'anio': 2019L, 'familia': 'Cisco', 'mes': 'Agosto'}]

            tmp_data_chat_2_anio = ''
            tmp_data_chat_2_mes = ''
            tmp_data_chat_2_familias = []
            tmp_data_chat_2_familias_unicas = []
            tmp_data_chat_2_cantidades = []
            tmp_data_chat_2_data_x_anio = {}

            tmp_data_chat_2_familias = set([x['familia'] for x in data_chart_2])
            tmp_data_chat_2_meses = set([x['mes'] for x in data_chart_2])
            tmp_data_chat_2_anios = set([x['anio'] for x in data_chart_2])

            #agrupar en un diccionario por anios
            for anio in tmp_data_chat_2_anios:
                tmp = []
                for row in data_chart_2:
                    if row['anio'] == anio:
                        tmp.append(row)
                tmp_data_chat_2_data_x_anio[anio] = tmp

            for anio in tmp_data_chat_2_data_x_anio.keys():
                for data_anio in tmp_data_chat_2_data_x_anio[anio]:
                    pass

            for familia in tmp_data_chat_2_familias:
                for anio in tmp_data_chat_2_anios:
                    for mes in tmp_data_chat_2_meses:
                        for row in data_chart_2:
                            if anio == row['anio'] and mes == row['mes']:
                                if familia == row['anio']:
                                    print('')

            print(tmp_data_chat_2_meses)
            # print(tmp_data_chat_2_familias)


            response['result']['dataset_1'] = dataset_1
            response['result']['dataset_2'] = dataset_2
            response['result']['dataset_3'] = dataset_3
            response['result']['dataset_4'] = dataset_4
            response['result']['dataset_5'] = dataset_5
            response['result']['dataset_6'] = dataset_6

            response['result']['dataset_6_2'] = dataset_6_2
            response['result']['dataset_7_1'] = dataset_7_1
            # response['result']['dataset_7_3'] = dataset_7_3  dio error por json serializable datetime
            response['result']['dataset_100'] = dataset_100
            response['result']['dataset_100_1'] = dataset_100_1
            response['result']['dataset_100_2'] = dataset_100_2

        else:
            response['result'] = 'Parametros obligatorios: tickets_count, start_date y end_date'
            response['code'] = 400
            return render_to_response('americatel/tickets_graphics.html', {})
        res = HttpResponse(json.dumps(response, cls=DecimalEncoder), mimetype="application/json")
        res.headers = {'Content-Type': 'application/json'}
        return res


"""